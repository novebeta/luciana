<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    'theme' => 'extjs',
//        'preload' => array('log'),
    'sourceLanguage' => 'xx',
    'language' => 'en',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*'
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin',
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array('ext.giix-core',
            ),
        ),
    ),
    'components' => array(
        'CGridViewPlus' => array(
            'class' => 'components.CGridViewPlus',
        ),
        'user' => array(
            'loginUrl' => array('site/login'),
            'allowAutoLogin' => true,),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
//                '/' => 'site/index',
//                '<action:(contact|login|logout)>/*' => 'site/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
//        'excel' => array(
//            'class' => 'application.extensions.PHPExcel',
//        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=luciana;port=3306',
            'emulatePrepare' => true,
//            'tablePrefix' => 'pbu_',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ),
        'session' => array(
            'sessionName' => 'luciana',
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => false,
            'connectionID' => 'db',
            'sessionTableName' => 'session',
            'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'timeout' => 108000
        ),
        'errorHandler' => array(
            'errorAction' => '',),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Mailer' => 'smtp',
            'Port' => 587,
            'SMTPAuth' => true,
            'SMTPSecure' => 'tls',
        ),
//        'log' => array(
//            'class' => 'CLogRouter',
//            'routes' => array(array(
//                    'class' => 'CWebLogRoute',
//                    'levels' => 'error, warning',
//                ),
//            ),
//        ),
//        'ePdf' => array(
//            'class' => 'ext.yii-pdf.EYiiPdf',
//            'params' => array(
//                'mpdf' => array(
//                    'librarySourcePath' => 'application.vendors.mpdf.*',
//                    'constants' => array(
//                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
//                    ),
//                    'class' => 'mpdf',
//                ),
//                'HTML2PDF' => array(
//                    'librarySourcePath' => 'application.vendors.html2pdf.*',
//                    'classFile' => 'html2pdf.class.php',
//                )
//            ),
//        ),
    ),
    'params' => array(
        'Username' => '',
        'Password' => '',
        'emailInvoice' => '',
        'adminEmail' => '',
        'system_title' => '',
        'system_subtitle' => '',
        'compressKey' => '',
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
    ),
//    'behaviors' => array(
//        'onBeginRequest' => array('class' => 'application.components.RequireLogin'
//        )
//    ),
);
