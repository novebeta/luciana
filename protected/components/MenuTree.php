<?php
class MenuTree
{
    var $security_role;
    var $menu_users = "{text: 'User Manajement',
                                id: 'jun.UsersGrid',
                                leaf: true
                                },";
    var $security = "{text: 'Security Roles',
                                id: 'jun.SecurityRolesGrid',
                                leaf: true
                                },";
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    function getChildMaster()
    {
        $child = "";
        $child .= in_array(101, $this->security_role) ? "{
                            text: 'Pasien',
                            id: 'jun.PasienGrid',
                            leaf: true
                        }," : '';
        return $child;
    }
    function getMaster($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Master',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getTransaction($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Transaction',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildTransaction()
    {
        $child = "";
        $child .= in_array(201, $this->security_role) ? "{
                            text: 'Buat Pemeriksaan',
                            id: 'jun.PasienPeriksaGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(201, $this->security_role) ? "{
                            text: 'Daftar Pemeriksaan',
                            id: 'jun.PeriksaGrid',
                            leaf: true
                        }," : '';
        return $child;
    }
    function getReport($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Report',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildReport()
    {
        $child = "";
//        $child .= in_array(301, $this->security_role) ? "{
//                            text: 'Mutasi Stok',
//                            id: 'jun.ReportInventoryMovements',
//                            leaf: true
//                        }," : '';
        return $child;
    }
    function getAdministration($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Administration',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildAdministration()
    {
        $child = "";
        $child .= in_array(400, $this->security_role) ? "{
                            text: 'User Manajement',
                            id: 'jun.UsersGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(401, $this->security_role) ? "{
                            text: 'Security Roles',
                            id: 'jun.SecurityRolesGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(402, $this->security_role) ? "{
                            text: 'Backup',
                            id: 'jun.BackupRestoreWin',
                            leaf: true
                        }," : '';
        return $child;
    }
    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = "";
        $child .= in_array(001, $this->security_role) ? "{
                            text: 'Change Password',
                            id: 'jun.PasswordWin',
                            leaf: true
                        }," : '';
        $child .= in_array(002, $this->security_role) ? "{
                            text: 'Logout ($username)',
                            id: 'logout',
                            leaf: true
                        }," : '';
        return $child;
    }
    public function get_menu()
    {
        $username = Yii::app()->user->name;
        $data = "[";
        $data .= self::getMaster(self::getChildMaster());
        $data .= self::getTransaction(self::getChildTransaction());
        $data .= self::getReport(self::getChildReport());
        $data .= self::getAdministration(self::getChildAdministration());
        $data .= self::getGeneral();
        $data .= "]";
        return $data;
    }
}
