<?php
class U
{
    // ------------------------------------------------ Void ----------------------------------------------------------------
    static function get_voided($type)
    {
        $void = Yii::app()->db->createCommand()->select('id')->from('mt_voided')->where('type=:type',
            array(
                ':type' => $type
            ))->queryColumn();
        return $void;
    }
    static function get_max_type_no($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(type_no)")
            ->from("{{gl_trans}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }
    static function get_max_type_no_stock($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(trans_no)")
            ->from("{{stock_moves}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }
    // --------------------------------------------- Bank Trans -------------------------------------------------------------
    static function get_next_trans_no_bank_trans()
    {
//        $db = BankTrans::model()->getDbConnection();
        $total = app()->db->createCommand(
            "SELECT MAX(trans_no)
            FROM pbu_bank_trans WHERE type= :type")
            ->queryScalar(array('BANKTRANSFER' => BANKTRANSFER));
        return $total == false ? 0 : $total + 1;
    }
    static function get_next_trans_saldo_awal()
    {
        $db = GlTrans::model()->getDbConnection();
        $total = $db->createCommand(
            "SELECT MAX(type_no)
FROM mt_gl_trans WHERE type=" . SALDO_AWAL)->queryScalar();
        return $total == null ? 0 : $total + 1;
    }
    static function get_ledger_trans($from, $to)
    {
        $rows = Yii::app()->db->createCommand(
            "SELECT
            mt_gl_trans.tran_date,
            mt_gl_trans.type,
            mt_gl_trans.type_no,
            refs.reference,
            SUM(IF(mt_gl_trans.amount>0, mt_gl_trans.amount,0)) as amount,
            users.user_id
            FROM
            mt_gl_trans
            LEFT JOIN mt_refs as refs ON
            (mt_gl_trans.type=refs.type AND mt_gl_trans.type_no=refs.type_no),users
            WHERE mt_gl_trans.tran_date BETWEEN '$from' AND '$to'
            GROUP BY mt_gl_trans.tran_date,mt_gl_trans.type,
            mt_gl_trans.type_no,mt_gl_trans.users_id
            ")->queryAll();
        return $rows;
    }
    static function get_general_ledger_trans($from, $to)
    {
        $rows = Yii::app()->db->createCommand(
            "SELECT
            mt_gl_trans.type,
            mt_gl_trans.type_no,
            mt_gl_trans.tran_date,
            CONCAT(mt_chart_master.account_code,' ',mt_chart_master.account_name) as account,
            mt_gl_trans.amount
            FROM
            mt_gl_trans
            INNER JOIN mt_chart_master ON mt_gl_trans.account = mt_chart_master.account_code
            WHERE mt_gl_trans.tran_date BETWEEN '$from' AND '$to'
            ")->queryAll();
        return $rows;
    }
    static function get_bank_trans_view()
    {
        global $systypes_array;
        $bfw = U::get_balance_before_for_bank_account($_POST['trans_date_mulai'],
            $_POST['bank_act']);
        $arr['data'][] = array(
            'type' => 'Saldo Awal - ' . sql2date($_POST['trans_date_mulai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $bfw >= 0 ? number_format($bfw, 2) : '',
            'kredit' => $bfw < 0 ? number_format($bfw, 2) : '',
            'neraca' => '',
            'person' => ''
        );
        $credit = $debit = 0;
        $running_total = $bfw;
        if ($bfw > 0) {
            $debit += $bfw;
        } else {
            $credit += $bfw;
        }
        $result = U::get_bank_trans_for_bank_account($_POST['bank_act'],
            $_POST['trans_date_mulai'], $_POST['trans_date_sampai']);
        foreach ($result as $myrow) {
            $running_total += $myrow->amount;
            $jemaat = get_jemaat_from_user_id($myrow->users_id);
            $arr['data'][] = array(
                'type' => $systypes_array[$myrow->type],
                'ref' => $myrow->ref,
                'tgl' => sql2date($myrow->trans_date),
                'debit' => $myrow->amount >= 0 ? number_format($myrow->amount, 2)
                    : '',
                'kredit' => $myrow->amount < 0 ? number_format(-$myrow->amount,
                    2) : '',
                'neraca' => number_format($running_total, 2),
                'person' => $jemaat->real_name
            );
            if ($myrow->amount > 0) {
                $debit += $myrow->amount;
            } else {
                $credit += $myrow->amount;
            }
        }
        $arr['data'][] = array(
            'type' => 'Saldo Akhir - ' . sql2date($_POST['trans_date_sampai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $running_total >= 0 ? number_format($running_total, 2) : '',
            'kredit' => $running_total < 0 ? number_format(-$running_total, 2) : '',
            'neraca' => '', // number_format($debit + $credit, 2),
            'person' => ''
        );
        return $arr;
    }
    static function get_balance_before_for_bank_account($from, $bank_account = null)
    {
//        $db = BankTrans::model()->getDbConnection();
        $query = app()->db->createCommand();
        $query->select = "SUM(amount)";
        $query->from = "{{bank_trans}}";
        $query->where('trans_date < :from', array(':from' => $from));
        if ($bank_account != null) {
            $query->andWhere('id_bank = :id_bank', array(':id_bank' => $bank_account));
        }
        $total = $query->queryScalar();
        return $total ? $total : 0;
    }
    static function get_bank_trans_for_bank_account($bank_account, $from, $to)
    {
        $criteria = new CDbCriteria();
        if ($bank_account != null) {
            $criteria->addCondition("bank_act =" . $bank_account);
        }
        $criteria->addBetweenCondition("trans_date", $from, $to);
        $criteria->order = "trans_date, id";
        return BankTrans::model()->findAll($criteria);
    }
    static function get_prefs($name)
    {
        $criteria = new CDbCriteria();
        if ($name != null) {
            $criteria->addCondition("name ='$name'");
        } else {
            return null;
        }
        $prefs = SysPrefs::model()->find($criteria);
        return $prefs->value;
    }
    static function get_act_code_from_bank_act($bank_act)
    {
        $bank = Bank::model()->findByPk($bank_act);
        if ($bank != null) {
            return $bank->accountCode->account_code;
        } else {
            return false;
        }
    }
    static function get_sql_for_journal_inquiry($from, $to)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "gl_trans.tran_date,gl_trans.type,refs.reference,Sum(IF(amount>0, amount,0)) AS amount,
    comments.memo_,gl_trans.person_id,gl_trans.type_no")->from('gl_trans')->join(
            'comments',
            'gl_trans.type = comments.type AND gl_trans.type_no = comments.type_no')->Join(
            'refs',
            'gl_trans.type = refs.type AND gl_trans.type_no = refs.type_no')->where(
            "gl_trans.amount!=0 and gl_trans.tran_date >= '$from'
?  ?          AND gl_trans.tran_date <= '$to'")->group('gl_trans.type, gl_trans.type_no')->order(
            'tran_date desc')->queryAll();
        return $rows;
    }
    static function add_gl(
        $type,
        $trans_id,
        $date_,
        $ref,
        $account,
        $memo_,
        $comment_,
        $amount
    ) {
        $person_id = Yii::app()->user->getId();
        $is_bank_to = self::is_bank_account($account);
        self::add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount,
            $person_id);
        if ($is_bank_to) {
            self::add_bank_trans($type, $trans_id, $is_bank_to, $ref, $date_,
                $amount, $person_id);
        }
        self::add_comments($type, $trans_id, $date_, $comment_);
        // return $trans_id;
    }
    // --------------------------------------------- Gl Trans ---------------------------------------------------------------
    static function is_bank_account($account_code)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(":account_code" => $account_code);
        $bank_act = Bank::model()->find($criteria);
        if ($bank_act != null) {
            return $bank_act->id_bank;
        } else {
            return false;
        }
    }
    static function add_gl_trans(
        $type,
        $trans_id,
        $date_,
        $account,
        $memo_,
        $amount,
        $person_id
    ) {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        if (!$gl_trans->save()) {
            throw new Exception("Gagal menyimpan jurnal." . CHtml::errorSummary($gl_trans));
        }
    }
    static function add_bank_trans(
        $type,
        $trans_no,
        $bank_act,
        $ref,
        $date_,
        $amount,
        $person_id
    ) {
        $bank_trans = new BankTrans();
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        if (!$bank_trans->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
        }
    }
    static function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = new Comments();
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save()) {
                throw new Exception("Gagal menyimpan keterangan." . CHtml::errorSummary($comment));
            }
        }
    }
    static function add_stock_moves(
        $type,
        $trans_no,
        $tran_date,
        $barang_id,
        $batch,
        $tgl_exp,
        $qty,
        $reference,
        $price
    ) {
        $move = new StockMoves;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->tran_date = $tran_date;
        $move->price = $price;
        $move->reference = $reference;
        $move->qty = $qty;
        $move->barang_id = $barang_id;
        $move->batch = $batch;
        $move->tgl_exp = $tgl_exp;
        $move->id_user = Yii::app()->user->getId();
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
//        if ($qty < 0) {
//            $saldo = StockMoves::get_saldo_item_batch($barang_id, $batch, $tgl_exp);
//            if ($saldo < 0) {
//                /** @var $barang Barang */
//                $barang = Barang::model()->findAllByPk($barang_id);
//                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Stock moves')) .
//                    "Saldo tidak cukup untuk barang " . $barang->nama_barang . " BATCH " . $batch . " MFG_DATE " . $tgl_exp);
//            }
//        }
    }
    // --------------------------------------------- Comments ---------------------------------------------------------------
    static function get_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        return Comments::model()->find($criteria);
    }
    static function update_comments($type, $id, $date_, $memo_)
    {
        if ($date_ == null) {
            U::delete_comments($type, $id);
            U::add_comments($type, $id,
                Yii::app()->dateFormatter->format('yyyy-MM-dd', time()),
                $memo_);
        } else {
            $criteria = new CDbCriteria();
            $criteria->addCondition("type=" . $type);
            $criteria->addCondition("id=" . $id);
            $criteria->addCondition("date_=" . $date_);
            $comment = Comments::model()->find($criteria);
            $comment->memo_ = $memo_;
            $comment->save();
        }
    }
    static function delete_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        $comment = Comments::model()->find($criteria);
        $comment->delete();
    }
    // ---------------------------------------------- Report ----------------------------------------------------------------
    static function get_beban()
    {
        $rows = app()->db->createCommand("SELECT account_code FROM mt_chart_master WHERE account_code REGEXP '^5[1-9]'")->queryAll();
        return $rows;
    }
    static function get_daftar_master_konsumen($code, $nama, $phone, $hp, $hp2, $tempo, $status)
    {
        $query = app()->db->createCommand();
        $query->select("CONCAT(pk.konsumen_code,' ') `Kode Konsumen`, pk.konsumen_name `Nama Konsumen`,
        pk.phone Phone, pk.hp HP, pk.hp2 `HP 2`, pk.tempo Tempo, pk.address Alamat, pk.status `Status`");
        $query->from("{{konsumen}} pk");
        if ($code !== "") {
            $query->andWhere("konsumen_code like :konsumen_code", array(":konsumen_code" => $code . "%"));
        }
        if ($nama !== "") {
            $query->andWhere("konsumen_name like :konsumen_name", array(":konsumen_name" => "%" . $nama . "%"));
        }
        if ($phone !== "") {
            $query->andWhere("phone like :phone", array(":phone" => "%" . $phone . "%"));
        }
        if ($hp !== "") {
            $query->andWhere("hp like :hp", array(":hp" => "%" . $hp . "%"));
        }
        if ($hp2 !== "") {
            $query->andWhere("hp2 like :hp2", array(":hp2" => "%" . $hp2 . "%"));
        }
        if ($tempo !== "") {
            $query->andWhere("tempo like :tempo", array(":tempo" => "%" . $tempo . "%"));
        }
        if ($status !== "") {
            $query->andWhere("status like :status", array(":status" => "%" . $status . "%"));
        }
        return $query->queryAll(true);
    }
    static function get_mutasi_kas_ditangan($start_date, $end_date)
    {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('trans_date', $start_date, $end_date);
        $model = BankTrans::model()->findAll($criteria);
        return $model;
    }
    static function get_arr_kode_rekening_pengeluaran($code = "")
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type='" . Prefs::TypeCostAct() . "'");
        if ($code != "account_code" && $code != "") {
            $criteria->addCondition("account_code='$code'");
        }
        $model = ChartMaster::model()->findAll($criteria);
        $daftar = array();
        foreach ($model as $coderek) {
            $daftar[$coderek['account_code']] = $coderek['account_name'];
        }
        return $daftar;
    }
    static function get_pengeluaran_detil_kode_rekening(
        $start_date,
        $end_date,
        $code
    ) {
        $rows = Yii::app()->db->createCommand()->select(
            "a.tran_date,a.memo_,IF(a.amount > 0,a.amount,'') as debit,IF(a.amount < 0,-a.amount,'') as kredit")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code
    AND a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->leftJoin('mt_voided c', "a.type_no=c.id AND c.type=a.type")->where(
            "b.account_code=:code and a.type != :type and ISNULL(c.date_)",
            array(
                'code' => $code,
                'type' => VOID
            ))->order("a.tran_date")->queryAll();
        // ->where("b.account_code=:code",array('code'=>$code))
        return $rows;
    }
    static function get_pengeluaran_per_kode_rekening($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_code,b.account_name as nama_rekening,IFNULL(sum(a.amount),0) as total_beban")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code
    AND a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->where("b.account_type=:type and !b.inactive",
            array(
                ':type' => Prefs::TypeCostAct()
            ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }
    static function get_total_pengeluaran($start_date, $end_date, $code = "")
    {
        $kode = $code == "" ? "" : "and b.account_code = '$code'";
        $rows = Yii::app()->db->createCommand()->select("sum(a.amount) as total_beban")->from(
            "mt_gl_trans a")->join("mt_chart_master b",
            "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type $kode",
            array(
                ':start' => $start_date,
                ':end' => $end_date,
                ':type' => Prefs::TypeCostAct()
            ))->queryScalar();
        return $rows == null ? 0 : $rows;
    }
    static function get_detil_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_name as nama_rekening,IFNULL(-sum(a.amount),0) as total_pendapatan")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code and
        a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->where("b.account_type=:type and !b.inactive",
            array(
                ':type' => Prefs::TypePendapatanAct()
            ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }
    static function get_total_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select("-sum(a.amount) as total_pendapatan")->from(
            "mt_gl_trans a")->join("mt_chart_master b",
            "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type",
            array(
                ':start' => $start_date,
                ':end' => $end_date,
                ':type' => Prefs::TypePendapatanAct()
            ))->order("b.account_code")->queryScalar();
        return $rows == null ? 0 : $rows;
    }
    static function get_chart_master_beban()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type = " . Prefs::TypeCostAct());
        return ChartMaster::model()->findAll($criteria);
    }
    static function account_in_gl_trans($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = '$account'");
        $count = GlTrans::model()->count($criteria);
        return $count > 0;
    }
    static function account_used_bank($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = '$account'");
        $count = Bank::model()->count($criteria);
        return $count > 0;
    }
    static function report_new_customers($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT * FROM pbu_customers
            WHERE DATE(awal) >= :from AND DATE(AWAL) <= :to");
        return $comm->queryAll(true, array(':from' => $from, 'to' => $to));
    }
    static function report_biaya($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT nk.tgl, nk.doc_ref, nk.no_kwitansi,
            nk.keperluan,-nk.total total
            FROM pbu_kas AS nk
            WHERE nk.total < 0 AND DATE(tgl) >= :from AND DATE(tgl) <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_kartu_stok($barang_id, $from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT DATE(nsm.tran_date) tgl,nsm.reference doc_ref,nsm.batch,nsm.tgl_exp,
            0 awal,IF(nsm.qty >0,nsm.qty,0) masuk,IF(nsm.qty <0,-nsm.qty,0) keluar,
            0 akhir,nsm.qty FROM pbu_stock_moves AS nsm
            WHERE DATE(nsm.tran_date) >= :from AND DATE(nsm.tran_date) <= :to
            AND nsm.barang_id = :barang_id
            ORDER BY nsm.tran_date");
        return $comm->queryAll(true, array(
            ':barang_id' => $barang_id,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_terima_barang_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ptb.doc_ref,DATE_FORMAT(ptb.tgl,'%d/%m/%Y') tgl,pb.kode_barang,pb.nama_barang,
        ptbd.batch,DATE_FORMAT(ptbd.tgl_exp,'%d/%m/%Y') tgl_exp,ptbd.qty
        FROM pbu_terima_barang_details AS ptbd
        INNER JOIN pbu_barang AS pb ON ptbd.barang_id = pb.barang_id
        INNER JOIN pbu_terima_barang AS ptb ON ptbd.terima_barang_id = ptb.terima_barang_id
        WHERE ptb.tgl >= :from AND ptb.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_retur_beli_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ps.doc_ref,DATE_FORMAT(ps.tgl,'%d/%m/%Y') tgl,pc.kode_customer,
        pc.nama_customer,pb.kode_barang,pb.nama_barang,
        psd.qty,psd.batch,DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') tgl_exp
        FROM pbu_sjsim AS ps
        INNER JOIN pbu_sjsim_details AS psd ON psd.sjsim_id = ps.sjsim_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_int_details($from, $to, $grup_id = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $where = "AND pb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT ps.doc_ref,ps.doc_ref_inv,
        DATE_FORMAT(ps.tgl,'%d/%m/%Y') AS tgl,
        DATE_FORMAT(ps.tgl_jatuh_tempo,'%d/%m/%Y') AS tgl_jatuh_tempo,
        pc.kode_customer,pc.nama_customer,pb.kode_barang,pb.nama_barang,
        psd.qty,psd.batch,DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') AS tgl_exp,
        psd.price,psd.total
        FROM pbu_sj AS ps
        INNER JOIN pbu_sj_details AS psd ON psd.sj_id = ps.sj_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to $where
        ORDER BY ps.tgl");
        return $comm->queryAll(true, $param);
    }
    static function report_tax_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT ps.doc_ref,ps.doc_ref_inv,
        DATE_FORMAT(ps.tgl,'%d/%m/%Y') AS tgl,
        DATE_FORMAT(ps.tgl_jatuh_tempo,'%d/%m/%Y') AS tgl_jatuh_tempo,
        pc.kode_customer,pc.nama_customer,pb.kode_barang,pb.nama_barang,
        psd.qty,psd.batch,DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') AS tgl_exp,
        psd.price,psd.total
        FROM pbu_sjtax AS ps
        INNER JOIN pbu_sjtax_details AS psd ON psd.sjtax_id = ps.sjtax_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_credit_note_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT ps.doc_ref,
        DATE_FORMAT(ps.tgl,'%d/%m/%Y') AS tgl,pb.kode_barang,
        pb.nama_barang,psd.qty,psd.batch,
        DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') AS tgl_exp,
        psd.price,psd.total
        FROM pbu_credit_note AS ps
        INNER JOIN pbu_credit_note_details AS psd ON psd.credit_note_id = ps.credit_note_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_mutasi_stok($from, $to)
    {
        $comm = Yii::app()->db->createCommand(" SELECT nb.kode_barang, nb.nama_barang,nsm.batch,
		        SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) 'before',
        SUM(IF (nsm.qty > 0 AND nsm.type_no = 5 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) terima,
		SUM(IF (nsm.qty < 0 AND nsm.type_no = 9 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) sj,
		SUM(IF (nsm.qty < 0 AND nsm.type_no = 12 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) returbeli,
        SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) after
        FROM pbu_barang AS nb LEFT  JOIN pbu_stock_moves AS nsm ON (nsm.barang_id = nb.barang_id)
        GROUP BY nb.kode_barang, nb.nama_barang,nsm.batch ORDER BY nb.barang_id");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_history_pasien($pasien_id)
    {
        $comm = Yii::app()->db->createCommand("SELECT p.doc_ref,
        p.keluhan,p.riwayat_penyakit_sekarang,
        p.riwayat_penyakit_dahulu,
        p.periksa_fisik,p.periksa_penunjang,
        p.diagnosis,p.terapi,
        p.tdate,p.pasien_id,DATE_FORMAT(p.tgl,'%d %b %Y') AS tgl
        FROM periksa AS p
        WHERE p.pasien_id = :pasien_id
        ORDER BY tgl DESC");
        return $comm->queryAll(true, array(':pasien_id' => $pasien_id));
    }
    static function get_sales_trans_for_audit($date)
    {
        $comm = Yii::app()->db->createCommand("SELECT ns.salestrans_id,ns.doc_ref,
        nc.no_customer,nc.nama_customer,nba.nama_bank,Sum(nsd.total) AS total
        FROM nscc_salestrans AS ns
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        INNER JOIN nscc_bank AS nba ON ns.bank_id = nba.bank_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        WHERE nb.grup_id = 1 AND nsd.total >= 0 AND ns.salestrans_id NOT IN(SELECT na.audit_id FROM nscc_audit na)
        AND DATE(ns.tgl) = :date
        GROUP BY ns.doc_ref,nc.no_customer,nc.nama_customer,nba.nama_bank");
        return $comm->queryAll(true, array(':date' => $date));
    }
    static function get_list_audit($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ps.sj_id,ps.doc_ref,ps.total_tax total,pc.kode_customer,
        pc.nama_customer,ps.tgl
        FROM pbu_sj AS ps
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.adt <> 1 AND ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
}