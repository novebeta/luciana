<?php
Yii::import('application.models._base.BasePasienPeriksaView');
class PasienPeriksaView extends BasePasienPeriksaView
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'periksa_id';
    }
}