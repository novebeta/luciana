<?php

Yii::import('application.models._base.BaseUsers');

class Users extends BaseUsers
{
    public function beforeValidate()
    {
        if ($this->id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id = $uuid;
        }
        return parent::beforeValidate();
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function is_available_role($role)
    {
        $sections = explode(',',$this->securityRoles->sections);
        return in_array($role, $sections);
    }
}