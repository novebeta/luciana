<?php
Yii::import('application.models._base.BasePasien');

class Pasien extends BasePasien
{
    public function beforeValidate(){
        if ($this->pasien_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pasien_id = $uuid;
        }
        return parent::beforeValidate();
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}