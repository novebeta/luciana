<?php
Yii::import('application.models._base.BaseSysTypes');

class SysTypes extends BaseSysTypes
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->sys_types_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sys_types_id = $uuid;
        }
        return parent::beforeValidate();
    }
}