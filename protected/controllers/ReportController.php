<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public $is_excel;
    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->logo = bu() . "/images/logo.png";
        $this->TBS = new clsTinyButStrong;
        $this->layout = "report";
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        }
        error_reporting(E_ERROR);
    }
    public function actionPrintTerimaBarang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            /** @var $tb TerimaBarang */
            $tb = $this->loadModel($_POST['terima_barang_id'], 'TerimaBarang');
            //            $sj = new Sj;
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'in.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $sjd = $tb->report_terima_barang();
            $this->TBS->MergeField('header', array(
                'doc_ref' => $tb->doc_ref,
                'tgl' => sql2date($tb->tgl),
                'tanda_terima' => $tb->tanda_terima
            ));
            for ($i = count($sjd); $i <= 22; $i++) {
                $sjd[] = array(
                    'kode_barang' => null,
                    'nama_barang' => null,
                    'batch' => null,
                    'qty' => null,
                    'sat' => null,
                    'tgl_exp' => null,
                    'ket' => null
                );
            }
//                    foreach ($sjd as $key => $row) {
//                        $this->TBS->MergeField("$key", $row);
//                    }
            $this->TBS->MergeBlock('0', $sjd);
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TB$tb->doc_ref.xls");
        }
    }
    public function actionPrintSuratJalan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            /** @var $sj SJ */
            $sj = $this->loadModel($_POST['sj_id'], 'Sj');
            switch ($_POST['type']) {
                case 'btnPrintSJ':
                    //            $sj = new Sj;
                    if ($this->format == 'excel') {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sj.xml');
                    }
                    $this->TBS->SetOption('noerr', true);
                    $sjd = $sj->report_surat_jalan();
                    $this->TBS->MergeField('header', array(
                        'doc_ref' => $sj->doc_ref,
                        'tgl' => sql2date($sj->tgl),
                        'nama_customer' => $sj->customer->nama_customer,
                        'address' => $sj->customer->address
                    ));
                    for ($i = count($sjd); $i <= 22; $i++) {
                        $sjd[] = array(
                            'kode_barang' => null,
                            'nama_barang' => null,
                            'batch' => null,
                            'qty' => null,
                            'sat' => null,
                            'tgl_exp' => null,
                            'ket' => null
                        );
                    }
//                    foreach ($sjd as $key => $row) {
//                        $this->TBS->MergeField("$key", $row);
//                    }
                    $this->TBS->MergeBlock('0', $sjd);
                    if ($this->format == 'excel') {
                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SJ$sj->doc_ref.xls");
                    } else {
                        $this->TBS->Show();
                    }
                    break;
                case 'btnPrintSJInt':
                    //            $sj = new Sj;
                    if ($this->format == 'excel') {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sjint.xml');
                    }
                    $this->TBS->SetOption('noerr', true);
                    $sjd = $sj->report_invoice_int();
                    $subtotal = $sj->total;
                    $ppn = $subtotal * 0.1;
                    $total = $subtotal + $ppn;
                    $this->TBS->MergeField('header', array(
                        'doc_ref' => $sj->doc_ref_inv,
                        'tgl' => sql2date($sj->tgl, "dd M yyyy"),
                        'nama_customer' => $sj->customer->nama_customer,
                        'address' => $sj->customer->address,
                        'tgl_cetak' => date('d/m/Y'),
                        'tgl_jatuh_tempo' => sql2date($sj->tgl_jatuh_tempo, "dd M yyyy"),
                        'subtotal' => $subtotal,
                        'dpp' => $subtotal,
                        'ppn' => $ppn,
                        'total' => $total
                    ));
                    $this->TBS->MergeBlock('0', $sjd);
                    if ($this->format == 'excel') {
                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InvoiceInt$sj->doc_ref.xls");
                    } else {
                        $this->TBS->Show();
                    }
                    break;
            }
        }
    }
    public function actionPrintSuratJalanTax()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $sjtax = $this->loadModel($_POST['sjtax_id'], 'Sjtax');
            switch ($_POST['type']) {
                case 'btnPrintSJTax':
                    //            $sj = new Sj;
                    if ($this->format == 'excel') {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sjtax.xml');
                    }
                    $this->TBS->SetOption('noerr', true);
                    $sjd = $sjtax->report_surat_jalan_tax();
                    $this->TBS->MergeField('header', array(
                        'doc_ref' => $sjtax->doc_ref,
                        'tgl' => sql2date($sjtax->tgl),
                        'nama_customer' => $sjtax->customer->nama_customer,
                        'address' => $sjtax->customer->address
                    ));
                    for ($i = count($sjd); $i <= 22; $i++) {
                        $sjd[] = array(
                            'kode_barang' => null,
                            'nama_barang' => null,
                            'batch' => null,
                            'qty' => null,
                            'sat' => null,
                            'tgl_exp' => null,
                            'ket' => null
                        );
                    }
//                    foreach ($sjd as $key => $row) {
//                        $this->TBS->MergeField("$key", $row);
//                    }
                    $this->TBS->MergeBlock('0', $sjd);
                    if ($this->format == 'excel') {
                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SJTAX$sjtax->doc_ref.xls");
                    } else {
                        $this->TBS->Show();
                    }
                    break;
                case 'btnPrintINVTax':
                    //            $sj = new Sj;
                    if ($this->format == 'excel') {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'invtax.xml');
                    }
                    $this->TBS->SetOption('noerr', true);
                    $sjd = $sjtax->report_invoice_tax();
                    $subtotal = $sjtax->total;
                    $ppn = $subtotal * 0.1;
                    $total = $subtotal + $ppn;
                    $this->TBS->MergeField('header', array(
                        'doc_ref' => $sjtax->doc_ref_inv,
                        'tgl' => sql2date($sjtax->tgl),
                        'nama_customer' => $sjtax->customer->nama_customer,
                        'address' => $sjtax->customer->address,
                        'tgl_cetak' => date('d/m/Y'),
                        'tgl_jatuh_tempo' => sql2date($sjtax->tgl_jatuh_tempo),
                        'subtotal' => $subtotal,
                        'dpp' => $subtotal,
                        'ppn' => $ppn,
                        'total' => $total,
                        'npwp' => $sjtax->customer->npwp
                    ));
                    $this->TBS->MergeBlock('0', $sjd);
                    if ($this->format == 'excel') {
                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InvoiceTax$sjtax->doc_ref.xls");
                    } else {
                        $this->TBS->Show();
                    }
                    break;
            }
        }
    }
    public function actionHistoryPasien()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $pasien_id = $_POST['pasien_id'];
            $mutasi = U::report_history_pasien($pasien_id);
            $pasien = Pasien::model()->findByPk($pasien_id);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'HistoryPasien',
                'pagination' => false
            ));
            $this->render('HistoryPasien', array(
                'dp' => $dataProvider,
                'pasien' => $pasien
            ));
        }
    }
    public function actionInventoryCard()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $barang = Barang::model()->findByPk($barang_id);
            $saldo_awal = StockMoves::get_saldo_item_before($barang->kode_barang, $from);
            $row = U::report_kartu_stok($barang->barang_id, $from, $to);
            $stock_card = array();
            foreach ($row as $newrow) {
                $newrow['awal'] = $saldo_awal;
                $newrow['akhir'] += $newrow['awal'] + $newrow['qty'];
                $saldo_awal = $newrow['akhir'];
                $stock_card[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'InventoryCard',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryCard$from-$to.xls");
                echo $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
                $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang
                ));
//                Yii::app()->end();
            }
        }
    }
    public function actionTerimaBarangDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_terima_barang_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'TerimaBarangDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=TerimaBarangDetails$from-$to.xls");
                echo $this->render('TerimaBarangDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('TerimaBarangDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionReturBeliDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_retur_beli_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'ReturBeliDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ReturBeliDetails$from-$to.xls");
                echo $this->render('ReturBeliDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('ReturBeliDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionSJDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $mutasi = U::report_int_details($from, $to, $grup_id);
            if ($grup_id == "") {
                $grup_name = "Semua Grup";
            } else {
                /* @var $grup Grup */
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'SJDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SJDetails$from-$to.xls");
                echo $this->render('SJDetails', array(
                    'dp' => $dataProvider,
                    'grup' => $grup_name,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('SJDetails', array(
                    'dp' => $dataProvider,
                    'grup' => $grup_name,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionINVDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_int_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'INVDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=INVDetails$from-$to.xls");
                echo $this->render('INVDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('INVDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionSJTAXDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_tax_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'SJTAXDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SJTAXDetails$from-$to.xls");
                echo $this->render('SJTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('SJTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionINVTAXDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_tax_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'INVTAXDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=INVTAXDetails$from-$to.xls");
                echo $this->render('INVTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('INVTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionCreditNoteDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_credit_note_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'CreditNoteDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=CreditNoteDetails$from-$to.xls");
                echo $this->render('CreditNote', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('CreditNote', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
}