<?php
class PeriksaController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Periksa;
                $ref = new Reference($_POST['tgl']);
                $docref = $ref->get_next_reference(PERIKSA);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Periksa'][$k] = $v;
                }
                $_POST['Periksa']['doc_ref'] = $docref;
                $model->attributes = $_POST['Periksa'];
                $msg = "Data berhasil disimpan.";
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Periksa')) . CHtml::errorSummary($model));
                }
                $ref->save(PERIKSA, $model->periksa_id, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Periksa');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Periksa'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Periksa'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->periksa_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->periksa_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Periksa')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if (isset($_POST['pasien_id'])) {
            $criteria->addCondition('pasien_id = :pasien_id');
            $param[':pasien_id'] = $_POST['pasien_id'];
        }
        if (isset($_POST['kode_pasien'])) {
            $criteria->addCondition("kode_pasien like :kode_pasien");
            $param[':kode_pasien'] = '%' . $_POST['kode_pasien'] . '%';
        }
        if (isset($_POST['nama_pasien'])) {
            $criteria->addCondition("nama_pasien like :nama_pasien");
            $param[':nama_pasien'] = '%' . $_POST['nama_pasien'] . '%';
        }
        if (isset($_POST['sex'])) {
            $criteria->addCondition("sex like :sex");
            $param[':sex'] = '%' . $_POST['sex'] . '%';
        }
        if (isset($_POST['telp'])) {
            $criteria->addCondition("telp like :telp");
            $param[':telp'] = '%' . $_POST['telp'] . '%';
        }
        if (isset($_POST['address'])) {
            $criteria->addCondition("address like :address");
            $param[':address'] = '%' . $_POST['address'] . '%';
        }
        if (isset($_POST['keluhan'])) {
            $criteria->addCondition("keluhan like :keluhan");
            $param[':keluhan'] = '%' . $_POST['keluhan'] . '%';
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition("doc_ref like :doc_ref");
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = PasienPeriksaView::model()->findAll($criteria);
        $total = PasienPeriksaView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}