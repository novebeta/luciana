<?php
class PasienController extends GxController
{
    public function actionCreate()
    {
        $model = new Pasien;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Pasien'][$k] = $v;
            }
            $model->attributes = $_POST['Pasien'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pasien_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Pasien');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Pasien'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Pasien'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pasien_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pasien_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Pasien')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['pasien_id'])) {
            $criteria->addCondition('pasien_id = :pasien_id');
            $param[':pasien_id'] = $_POST['pasien_id'];
        }
        if (isset($_POST['kode_pasien'])) {
            $criteria->addCondition("kode_pasien like :kode_pasien");
            $param[':kode_pasien'] = '%' . $_POST['kode_pasien'] . '%';
        }
        if (isset($_POST['nama_pasien'])) {
            $criteria->addCondition("nama_pasien like :nama_pasien");
            $param[':nama_pasien'] = '%' . $_POST['nama_pasien'] . '%';
        }
        if (isset($_POST['sex'])) {
            $criteria->addCondition("sex like :sex");
            $param[':sex'] = '%' . $_POST['sex'] . '%';
        }
        if (isset($_POST['telp'])) {
            $criteria->addCondition("telp like :telp");
            $param[':telp'] = '%' . $_POST['telp'] . '%';
        }
        if (isset($_POST['address'])) {
            $criteria->addCondition("address like :address");
            $param[':address'] = '%' . $_POST['address'] . '%';
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = Pasien::model()->findAll($criteria);
        $total = Pasien::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}