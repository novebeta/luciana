<?php

define('COA_VOUCHER', '431050');
define('COA_GRUP_BIAYA', '611000');
define('COA_GRUP_HUTANG', '211000');
define('COA_GRUP_PENDAPATAN', '700000');
define('COA_GRUP_BANK', '112000');
define('COA_PERSEDIAAN', '114101');
define('COA_VAT', '212020');
define('COA_GRUP_KAS', '111000');
define('COA_BIAYA_ADM_BANK', '710040');
define('COA_VAT_BELI', '115020');
define('COA_SALES_RETURN', '421010');
define('COA_SALES_GRUP', '400000');
define('COA_PURCHASE_GRUP', '511000');
define('COA_HPP_GRUP', '521000');
define('COA_TRANDE_RECEIVABLES', '113000');
define('COA_FEE_CARD', '611240');
define('COA_LABA_RUGI', '350000');
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


define('PERIKSA', 1);
define('MYSQL', 'E:\xampp\mysql\bin\mysql');
define('MYSQLDUMP', 'E:\xampp\mysql\bin\mysqldump');
define('GZIP', 'E:\xampp\7za');
global $systypes_array;
$systypes_array = array(
    PERIKSA => "Periksa",
);
/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * @param $number
 * @return int
 */

function is_angka($number)
{
    if ($number == "") {
        return false;
    }
    switch (gettype($number)) {
        case "NULL":
            return false;
        case "resource":
            return false;
        case "object":
            return false;
        case "array":
            return false;
        case "unknown type":
            return false;
    }
    return preg_match("/^-?([\$]?)([0-9,\s]*\.?[0-9]{0,2})$/", $number);
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

function dbTrans()
{
    return Yii::app()->db->beginTransaction();
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
    static $baseUrl;
    if ($baseUrl === null) {
        $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    }
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name)
{
    return Yii::app()->params[$name];
}

function Encrypt($string)
{
    return base64_encode(Yii::app()->getSecurityManager()->encrypt($string));
}

function Decrypt($string)
{
    return Yii::app()->getSecurityManager()->decrypt(base64_decode($string));
}

function generatePassword($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function date2longperiode($date, $format)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->format($format, $timestamp);
}

function period2date($month, $year)
{
    $timestamp = DateTime::createFromFormat('d/m/Y', "01/$month/$year");
    $start = $timestamp->format('Y-m-d');
    $end = $timestamp->format('Y-m-t');
    return array('start' => $start, 'end' => $end);
}

function get_jemaat_from_user($nij)
{
    return Jemaat::model()->findByPk($nij);
}

function get_number($number)
{
    return str_replace(",", "", $number);
}

function get_jemaat_from_user_id($id)
{
    $user = Users::model()->findByPk($id);
    if ($user == null) {
        return false;
    } else {
        return get_jemaat_from_user($user->nij);
    }
}

function sql2date($date, $format = 'yyyy-MM-dd')
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    return Yii::app()->dateFormatter->format($format, $timestamp);
}

function date2sql($date)
{
    $timestamp = CDateTimeParser::parse($date, 'dd/MM/yyyy');
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', $timestamp);
}

function sql2long_date($date)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->formatDateTime($timestamp, 'long', false);
}

function get_date_tomorrow()
{
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', time() + (1 * 24 * 60 * 60));
}

function get_time_now()
{
    return Yii::app()->dateFormatter->format('HH:mm:ss', time());
}

function get_date_today($format = 'yyyy-MM-dd')
{
    return Yii::app()->dateFormatter->format($format, time());
}

function Now($formatDate = 'yyyy-MM-dd')
{
    return get_date_today($formatDate) . ' ' . get_time_now();
}

function percent_format($value, $decimal = 0)
{
    return number_format($value * 100, $decimal) . '%';
}

function curr_format($value, $decimal = 0)
{
    return "Rp" . number_format($value * 100, $decimal);
}

function acc_format($value, $decimal = 0)
{
    $normalize = $value < 0 ? -$value : $value;
    $print = number_format($normalize, $decimal);
    return $value < 0 ? "($print)" : $print;
}

function round_up($number, $precision = 2)
{
    $number = round($number, $precision);
    $fig = (int)str_pad('1', $precision, '0');
    return (ceil($number * $fig) / $fig);
}

function round_down($number, $precision = 2)
{
    $number = round($number, $precision);
    $fig = (int)str_pad('1', $precision, '0');
    return (floor($number * $fig) / $fig);
}

function mysql2excel($myql_date)
{
    return strtotime($myql_date) + ((strtotime('1970-01-01') - strtotime('1900-01-01')) * 86400);
}

function yiiparam($name, $default = null)
{
    if (isset(Yii::app()->params[$name])) {
        return Yii::app()->params[$name];
    } else {
        return $default;
    }
}

function is_connected($url, $port)
{
    $connected = @fsockopen($url, $port);
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function mailsend($username, $password, $to, $from, $subject, $message, $attachdata, $filename)
{
    $mail = Yii::app()->Smtpmail;
    $mail->SMTPDebug = false;
    $mail->SetFrom($from, "PBU 2");
    $mail->Username = $username;
    $mail->Password = $password;
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->Timeout       =   60; // set the timeout (seconds)
    $mail->SMTPKeepAlive = true; // don't close the connection between messages
    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->AddAddress($to);
    $mail->AddStringAttachment($attachdata, $filename);
    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    } else {
        return 'OK';
    }
    $mail->SmtpClose();
}

function is_report_excel()
{
    return (isset($_POST['format']) && $_POST['format'] == 'excel');
}

function format_number_report($num, $digit = 0)
{
    return (isset($_POST['format']) && $_POST['format'] == 'excel') ? $num : number_format($num, $digit);
}