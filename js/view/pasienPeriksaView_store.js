jun.PasienPeriksaViewstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PasienPeriksaViewstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PasienPeriksaViewStoreId',
            url: 'PasienPeriksaView',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kode_pasien'},
                {name: 'sex'},
                {name: 'tgl_lahir'},
                {name: 'ktp'},
                {name: 'address'},
                {name: 'telp'},
                {name: 'no_bpjs'},
                {name: 'riwayat_alergi'},
                {name: 'nama_pasien'},
                {name: 'periksa_id'},
                {name: 'doc_ref'},
                {name: 'keluhan'},
                {name: 'riwayat_penyakit_sekarang'},
                {name: 'riwayat_penyakit_dahulu'},
                {name: 'periksa_fisik'},
                {name: 'periksa_penunjang'},
                {name: 'diagnosis'},
                {name: 'terapi'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'pasien_id'},
                {name: 'tgl'}
            ]
        }, cfg));
    }
});
jun.rztPasienPeriksaView = new jun.PasienPeriksaViewstore();
//jun.rztPasienPeriksaView.load();
