jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 400,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "101"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "102"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Wilayah",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Harga Beli",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "104"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Harga Jual",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Customer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Barang Jadi Masuk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "201"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Barang Jadi Keluar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "202"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Print Invoice",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "203"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Mutasi Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "301"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Barang Masuk Detail",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Barang Keluar Detail",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Invoice Barang Keluar Detail",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Kartu Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Kolom Harga Kartu Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "306"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Backup",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Setting",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            },
                            {
                                fieldLabel: "",
                                labelSeparator: "",
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "002"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/";
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                timeOut: 1e3,
                scope: this,
                success: function (a, b) {
                    jun.rztSecurityRoles.reload();
                    var c = Ext.decode(b.response.responseText);
                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});