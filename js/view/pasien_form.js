jun.PasienWin = Ext.extend(Ext.Window, {
    title: 'Pasien',
    modez: 1,
    width: 400,
    height: 485,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Pasien',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. RM',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_pasien',
                        id: 'kode_pasienid',
                        ref: '../kode_pasien',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Pasien',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_pasien',
                        id: 'nama_pasienid',
                        ref: '../nama_pasien',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    new jun.cmbGender({
                        fieldLabel: 'Jenis Kelamin',
                        name: 'sex',
                        hiddenName: "sex",
                        ref: '../sex',
                        anchor: '100%'
                    }),
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_lahir',
                        fieldLabel: 'Tgl Lahir',
                        name: 'tgl_lahir',
                        id: 'tgl_lahirid',
                        format: 'd M Y',
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Telp',
                        hideLabel: false,
                        //hidden:true,
                        name: 'telp',
                        id: 'telpid',
                        ref: '../telp',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. KTP',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ktp',
                        id: 'ktpid',
                        ref: '../ktp',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. BPJS',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_bpjs',
                        id: 'no_bpjsid',
                        ref: '../no_bpjs',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Alamat',
                        hideLabel: false,
                        //hidden:true,
                        name: 'address',
                        id: 'addressid',
                        ref: '../address',
                        maxLength: 100,
                        //allowBlank: 1,
                        height: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Riwayat Alergi',
                        hideLabel: false,
                        //hidden:true,
                        name: 'riwayat_alergi',
                        id: 'riwayat_alergiid',
                        ref: '../riwayat_alergi',
                        maxLength: 600,
                        //allowBlank: 1,
                        height: 100,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PasienWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Pasien/update/id/' + this.id;
        } else {
            urlz = 'Pasien/create/';
        }
        Ext.getCmp('form-Pasien').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPasien.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Pasien').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});