jun.SecurityRolesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SecurityRolesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SecurityRolesStoreId',
            url: 'SecurityRoles',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'security_roles_id'},
                {name: 'role'},
                {name: 'ket'},
                {name: 'sections'},
                {name: '001'},
                {name: '002'},
                {name: '100'},
                {name: '101'},
                {name: '102'},
                {name: '103'},
                {name: '104'},
                {name: '105'},
                {name: '106'},
                {name: '107'},
                {name: '200'},
                {name: '201'},
                {name: '202'},
                {name: '203'},
                {name: '300'},
                {name: '301'},
                {name: '302'},
                {name: '303'},
                {name: '304'},
                {name: '305'},
                {name: '306'},
                {name: '307'},
                {name: '308'},
                {name: '400'},
                {name: '401'}
            ]
        }, cfg));
    }
});
jun.rztSecurityRoles = new jun.SecurityRolesstore();
//jun.rztSecurityRoles.load();
