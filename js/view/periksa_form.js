jun.PeriksaWin = Ext.extend(Ext.Window, {
    title: 'Pemeriksaan',
    modez: 1,
    width: 600,
    height: 625,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Periksa',
                labelWidth: 200,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        //allowBlank: ,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Pasien',
                        store: jun.rztPasienCmp,
                        forceSelection: true,
                        hiddenName: 'pasien_id',
                        valueField: 'pasien_id',
                        ref: '../pasien',
                        displayField: 'nama_pasien',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Keluhan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'keluhan',
                        id: 'keluhanid',
                        ref: '../keluhan',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Riwayat Penyakit Sekarang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'riwayat_penyakit_sekarang',
                        id: 'riwayat_penyakit_sekarangid',
                        ref: '../riwayat_penyakit_sekarang',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Riwayat Penyakit Dahulu',
                        hideLabel: false,
                        //hidden:true,
                        name: 'riwayat_penyakit_dahulu',
                        id: 'riwayat_penyakit_dahuluid',
                        ref: '../riwayat_penyakit_dahulu',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Pemeriksaan Fisik',
                        hideLabel: false,
                        //hidden:true,
                        name: 'periksa_fisik',
                        id: 'periksa_fisikid',
                        ref: '../periksa_fisik',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Pemeriksaan Penunjang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'periksa_penunjang',
                        id: 'periksa_penunjangid',
                        ref: '../periksa_penunjang',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Diagnosis',
                        hideLabel: false,
                        //hidden:true,
                        name: 'diagnosis',
                        id: 'diagnosisid',
                        ref: '../diagnosis',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Terapi',
                        hideLabel: false,
                        //hidden:true,
                        name: 'terapi',
                        id: 'terapiid',
                        ref: '../terapi',
                        anchor: '100%'
                        //allowBlank: 1
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PeriksaWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Periksa/update/id/' + this.id;
        } else {
            urlz = 'Periksa/create/';
        }
        Ext.getCmp('form-Periksa').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPeriksa.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Periksa').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});