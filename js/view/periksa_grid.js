jun.PeriksaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Daftar Pemeriksaan",
    id: 'docs-jun.PeriksaGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. RM',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_pasien',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_pasien',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Jenis Kelamin',
            sortable: true,
            resizable: true,
            dataIndex: 'sex',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Telp',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'address',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Keluhan',
            sortable: true,
            resizable: true,
            dataIndex: 'keluhan',
            width: 100,
            filter: {xtype: "textfield"}
        }//,
        //{
        //    header: 'riwayat_penyakit_sekarang',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'riwayat_penyakit_sekarang',
        //    width: 100
        //},
        //{
        //    header: 'riwayat_penyakit_dahulu',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'riwayat_penyakit_dahulu',
        //    width: 100
        //},
        //{
        //    header: 'periksa_fisik',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'periksa_fisik',
        //    width: 100
        //}
        /*
         {
         header:'periksa_penunjang',
         sortable:true,
         resizable:true,
         dataIndex:'periksa_penunjang',
         width:100
         },
         {
         header:'diagnosis',
         sortable:true,
         resizable:true,
         dataIndex:'diagnosis',
         width:100
         },
         {
         header:'terapi',
         sortable:true,
         resizable:true,
         dataIndex:'terapi',
         width:100
         },
         {
         header:'pasien_id',
         sortable:true,
         resizable:true,
         dataIndex:'pasien_id',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztPeriksa;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                //{
                //    xtype: 'button',
                //    text: 'Add Pemeriksaan',
                //    ref: '../btnAdd'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                {
                    xtype: 'button',
                    text: 'Lihat/Ubah Pemeriksaan',
                    ref: '../btnEdit'
                }
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'Hapus',
                //    ref: '../btnDelete'
                //}
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PeriksaGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pemeriksaan.");
            return;
        }
        var idz = selectedz.json.periksa_id;
        var form = new jun.PeriksaWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Periksa/delete/id/' + record.json.periksa_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPeriksa.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.PasienPeriksaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Buat Pemeriksaan",
    id: 'docs-jun.PasienGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. RM',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_pasien',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_pasien',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Telp',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'address',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztPasienPeriksa;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Pemeriksaan',
                    ref: '../btnAdd'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PasienPeriksaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadEditForm, this);
        //this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztPasienCmp.baseParams = {
            pasien_id: this.record.data.pasien_id
        };
        jun.rztPasienCmp.load();
        jun.rztPasienCmp.baseParams = {};
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pasien.");
            return;
        }
        this.record.data.pasien_id = selectedz.json.pasien_id;
        var form = new jun.PeriksaWin({modez: 0});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});

