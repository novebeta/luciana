jun.Pasienstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Pasienstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PasienStoreId',
            url: 'Pasien',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pasien_id'},
                {name: 'kode_pasien'},
                {name: 'nama_pasien'},
                {name: 'sex'},
                {name: 'tgl_lahir'},
                {name: 'ktp'},
                {name: 'address'},
                {name: 'telp'},
                {name: 'no_bpjs'},
                {name: 'riwayat_alergi'}
            ]
        }, cfg));
    }
});
jun.rztPasien = new jun.Pasienstore();
jun.rztPasienCmp = new jun.Pasienstore();
jun.rztPasienPeriksa = new jun.Pasienstore();
jun.rztPasienPeriksaCmp = new jun.Pasienstore();
//jun.rztPasien.load();
