jun.ReportInventoryMovements = Ext.extend(Ext.Window, {
    title: "Inventory Movements",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovements",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryCard = Ext.extend(Ext.Window, {
    title: "Kartu Stok",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Item',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBarangCmp,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTerimaBarangDetails = Ext.extend(Ext.Window, {
    title: "Penerimaan Barang Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTerimaBarangDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTerimaBarangDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().url = "Report/TerimaBarangDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTerimaBarangDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().url = "Report/TerimaBarangDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportTerimaBarangDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportReturBeliDetails = Ext.extend(Ext.Window, {
    title: "Retur Beli Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportReturBeliDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturBeliDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturBeliDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturBeliDetails").getForm().url = "Report/ReturBeliDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturBeliDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportReturBeliDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturBeliDetails").getForm().url = "Report/ReturBeliDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportReturBeliDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSJDetails = Ext.extend(Ext.Window, {
    title: "Surat Jalan Internal Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 185,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSJDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Grup',
                        store: jun.rztGrupCmp,
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        emptyText: "Semua Grup",
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        jun.ReportSJDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSJDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJDetails").getForm().url = "Report/SJDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSJDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSJDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJDetails").getForm().url = "Report/SJDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSJDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportINVDetails = Ext.extend(Ext.Window, {
    title: "Invoice Internal Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportINVDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportINVDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportINVDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVDetails").getForm().url = "Report/INVDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportINVDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportINVDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVDetails").getForm().url = "Report/INVDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportINVDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSJTAXDetails = Ext.extend(Ext.Window, {
    title: "Surat Jalan Pajak Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSJTAXDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSJTAXDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSJTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJTAXDetails").getForm().url = "Report/SJTAXDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSJTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSJTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJTAXDetails").getForm().url = "Report/SJTAXDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSJTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportINVTAXDetails = Ext.extend(Ext.Window, {
    title: "Invoice Pajak Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportINVTAXDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportINVTAXDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportINVTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVTAXDetails").getForm().url = "Report/INVTAXDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportINVTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportINVTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVTAXDetails").getForm().url = "Report/INVTAXDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportINVTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportCreditNoteDetails = Ext.extend(Ext.Window, {
    title: "Credit Note Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCreditNoteDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportCreditNoteDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().url = "Report/CreditNoteDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportCreditNoteDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().url = "Report/CreditNoteDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportCreditNoteDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});