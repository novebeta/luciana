jun.PasienGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pasien",
    id: 'docs-jun.PasienGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. RM',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_pasien',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_pasien',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Jenis Kelamin',
            sortable: true,
            resizable: true,
            dataIndex: 'sex',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Telp',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'address',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztPasien;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Pasien Baru',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Data Pasien',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show History Pasien',
                    ref: '../btnHistory'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportHistoryPasien",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "pasien_id",
                            ref: "../../pasien_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "html",
                            ref: "../../format"
                        }
                    ]
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Pasien',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PasienGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnHistory.on('Click', this.historyPasien, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    historyPasien: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pasien");
            return;
        }
        Ext.getCmp("form-ReportHistoryPasien").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportHistoryPasien").getForm().url = "Report/HistoryPasien";
        this.pasien_id.setValue(selectedz.json.pasien_id);
        var form = Ext.getCmp('form-ReportHistoryPasien').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PasienWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pasien_id;
        var form = new jun.PasienWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data pasien ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Pasien");
            return;
        }
        Ext.Ajax.request({
            url: 'Pasien/delete/id/' + record.json.pasien_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPasien.reload();
                jun.rztPasienCmp.reload();
                jun.rztPasienPeriksa.reload();
                jun.rztPasienPeriksaCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
