<html>
<head>
    <title>Upload Restore File</title>
</head>
<body>
<h1>Please upload an restore file</h1>
<div class="form">
    <?php
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'topic-form',
        'enableAjaxValidation'=>false,
//        'action'=> 'site/RestoreAll',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <div class="row">
        <input type="hidden"/>
        Upload this file: <input name="userfile" type="file" />
        <input type="submit" value="Send File" />
    </div>
    <?php $this->endWidget($form); ?>
</div>
</body>
</html>