<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link rel="shortcut icon" href="<?php echo bu();?>/images/icon-natasha.gif" />
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/default.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
        <style>
            #drop {
                border: 2px dashed #BBBBBB;
                border-radius: 5px;
                color: #BBBBBB;
                font: 20pt bold, "Vollkorn";
                padding: 25px;
                text-align: center;
            }
            * {
                font-size: 12px;
                font-family: Candara;
            }

            @media screen and (-webkit-min-device-pixel-ratio: 0) {
                .x-grid3-cell, /* Normal grid cell */
                .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                    box-sizing: border-box;
                }
            }
        </style>
    </head>
    <body>
        <script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
        <script>
            Ext.namespace('jun');
            var SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
            var SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
            var SYSTEM_LOGO = '<img src="<?=bu(); ?>/images/logo.png" alt=""/>';
            var LOGOUT = false;
            var DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
            function goodbye(e) {
                if(!LOGOUT){
                    if(!e) e = window.event;
                    e.cancelBubble = true;
                    e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
                    if (e.stopPropagation) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
            }
            window.onbeforeunload=goodbye;
        </script>
<?php echo $content; ?>
        <script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
    </body>
</html>
