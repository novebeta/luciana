<html>
<?php if (!$this->is_excel): ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <style type="text/css">
            body {
                font-family: helvetica, tahoma, verdana, sans-serif;
                padding: 20px;
                padding-top: 32px;
                font-size: 13px;
                background-color: #f4f4f4; !important;
            }

            .grid-view table.items {
                background: white;
                border-collapse: collapse;
                width: 100%;
                border: 1px #a4a4a4 solid !important;
            }

            .grid-view table.items th, .grid-view table.items td {
                font-size: 0.9em;
                border: 1px white solid;
                border-color: #5b5b5b #5b5b5b #5b5b5b !important;
                padding: 0.3em;
            }

            .grid-view table.items th {
                background: #E4E4E4 !important;
                text-align: center;
                color: #111111;
            }

            .grid-view table.items th a {
                font-weight: bold;
                text-decoration: none;
            }

            .grid-view table.items th a:hover {
                color: #FFF;
            }

            .grid-view table.items tr.even {
                background: white;
            }

            .grid-view table.items tr.odd {
                background: #EFEFEF;
            }

            .grid-view table.items tr.selected {
                background: #f2f2f2;
                border-color: #5b5b5b;
            }

            .grid-view table.items tr:hover.selected {
                border-color: #5b5b5b;
                background: #f2f2f2;
            }

            .grid-view table.items tbody tr:hover {
                background: #f2f2f2;
            }

            .grid-view .link-column img {
                border: 0;
            }

            .grid-view .button-column {
                text-align: center;
                width: 60px;
            }

            .grid-view .button-column img {
                border: 0;
            }

            .grid-view .checkbox-column {
                width: 15px;
            }

            .grid-view .summary {
                margin: 0 0 5px 0;
                text-align: right;
            }

            .grid-view .pager {
                margin: 5px 0 0 0;
                text-align: right;
            }

            .grid-view .empty {
                font-style: italic;
            }

            .grid-view .filters input,
            .grid-view .filters select {
                width: 100%;
                border: 1px solid #ccc;
            }

            /* grid border */
            .grid-view table.items th, .grid-view table.items td {
                border: 1px solid #DDDDDD !important;
            }

            /* disable selection for extrarows */
            .grid-view td.extrarow {
                background: none repeat scroll 0 0 #F8F8F8;
            }

            .subtotal {
                font-size: 14px;
                color: brown;
                font-weight: bold;
            }
        </style>
    </head>
<?php endif ?>
<body>
<?php echo $content; ?>
</body>
</html>