<?
/** @var $pasien Pasien */
?>
<h1>History Pasien</h1>
<h3>No. Rekam Medik : <?=$pasien->kode_pasien?></h3>
<h3>Nama Pasien : <?=$pasien->nama_pasien?></h3>
<h3>Riwayat Alergi : <?=$pasien->riwayat_alergi?></h3>
<?
$this->pageTitle = 'History Pasien';
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Doc. Ref',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'TGL',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Keluhan',
            'name' => 'keluhan'
        ),
        array(
            'header' => 'Riwayat Penyakit Sekarang',
            'name' => 'riwayat_penyakit_sekarang'
        ),
        array(
            'header' => 'Riwayat Penyakit Dahulu',
            'name' => 'riwayat_penyakit_dahulu'
        ),
        array(
            'header' => 'Pemeriksaan Fisik',
            'name' => 'periksa_fisik'
        ),
        array(
            'header' => 'Pemeriksaan Penunjang',
            'name' => 'periksa_penunjang'
        ),
        array(
            'header' => 'Diagnosis',
            'name' => 'diagnosis'
        ),
        array(
            'header' => 'Terapi',
            'name' => 'terapi'
        )
    )
));
?>