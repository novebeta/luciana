<h1>Inventory Card</h1>
<h2>ITEM : <?=$item->kode_barang?> - <?=$item->nama_barang?></h2>
<h3>FROM : <?=$from?></h3>
<h3>TO : <?=$to?></h3>
<?
$this->pageTitle = 'Inventory Card';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Tanggal',
            'name' => 'tgl'
        ),
        array(
            'header' => 'No. Referensi',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Harga Beli',
            'name' => 'price',
            'visible' => $user->is_available_role(306),
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Batch',
            'name' => 'batch'
        ),
        array(
            'header' => 'MFG Date',
            'name' => 'tgl_exp'
        ),
        array(
            'header' => 'Saldo Awal',
            'name' => 'awal',
            'value' => function ($data) {
                return number_format($data['awal']);
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Masuk',
            'name' => 'masuk',
            'value' => function ($data) {
                return number_format($data['masuk']);
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Keluar',
            'name' => 'keluar',
            'value' => function ($data) {
                return number_format($data['keluar']);
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Saldo Akhir',
            'name' => 'akhir',
            'value' => function ($data) {
                return number_format($data['akhir']);
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        )
    )
));
?>